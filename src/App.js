import React, { useState } from "react";
import "./App.css";

export default function App() {
  const [enteredTask, setEnteredTask] = useState();
  const [tasks, setTasks] = useState([]);
  const [tasksInProgress, setTasksInProgress] = useState([]);
  const [tasksInStop, setTasksInStop] = useState([]);

  const addTaskHandler = () => {
    const tempObject = {
      taskName: enteredTask
    };
    setTasks([...tasks, tempObject]);
  };

  const startHandler = (task) => {
    const tempObject = {
      taskName: task
    };
    const removedTaskList = tasks.filter(
      (eachTask) => eachTask.taskName !== task
    );
    setTasks(removedTaskList);
    setTasksInProgress([...tasksInProgress, tempObject]);
  };

  const deleteHandler = (removeTasks) => {
    const removedTaskList = tasks.filter(
      (eachTask) => eachTask.taskName !== removeTasks
    );
    setTasks(removedTaskList);
  };

  const stopHandler = (stopTask) => {
    const tempObject = {
      taskName: stopTask
    };
    const removedTaskList = tasks.filter(
      (eachTask) => eachTask.taskName !== stopTask
    );
    setTasksInProgress(removedTaskList);
    setTasksInStop([...tasksInStop, tempObject]);
  };
  return (
    <div className="App">
      <div className="add-task-container">
        <div>
          <h2>Add Task</h2>
          <input
            type="text"
            onChange={(event) => setEnteredTask(event.target.value)}
          />
          <button onClick={addTaskHandler}>Add Task</button>
        </div>
      </div>
      <div className="task-container">
        <div className="todo">
          <h1>To Do:</h1>
          {tasks.map((eachTask) => {
            return (
              <div className="toDoCard">
                <div className="toDoCardHeader"> {eachTask.taskName} </div>
                <div>
                  <button onClick={() => startHandler(eachTask.taskName)}>
                    {" "}
                    start{" "}
                  </button>
                  <button onClick={() => deleteHandler(eachTask.taskName)}>
                    {" "}
                    delete{" "}
                  </button>
                </div>
              </div>
            );
          })}
        </div>

        <div className="inprogress">
          <h1>In Progress:</h1>
          {tasksInProgress.map((eachTask) => {
            return (
              <div className="toDoCard">
                <div className="toDoCardHeader"> {eachTask.taskName} </div>
                <div>
                  <button> stop </button>
                  <button onClick={() => stopHandler(eachTask.taskName)}>
                    {" "}
                    finish{" "}
                  </button>
                </div>
              </div>
            );
          })}
        </div>

        <div className="done">
          <h1>Done:</h1>
          {tasksInStop.map((eachTask) => {
            return (
              <div className="toDoCard">
                <div className="toDoCardHeader"> {eachTask.taskName} </div>
                <div>
                  <button onClick={() => stopHandler(eachTask.taskName)}>
                    {" "}
                    Delete{" "}
                  </button>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}